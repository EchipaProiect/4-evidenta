# Evidenta Studentilor

# For the full project documentation just head to : bit.ly/documentatieace
# For the full system design just head to : bit.ly/diagrame
# For the Test cases just head to: 
 https://docs.google.com/spreadsheets/d/1TClQHwceGfUG6OgcIU5KtP6rjMdf9PpqGQ6hXuHaTa4/edit

The purpose of this project is to create a simple login for students and staff to be able to login see and add marks, and also find out the latest news.

# How it works
   - Just need to change your IP from AppConfig and then point it towards your database
   - Create a database called android_api and add this code 
```sh
use android_api
create table users(
id int(11) primary key auto_increment,
unique_id varchar(23) not null unique,
name varchar(50) not null,
email varchar(100) not null unique,
encrypted_password varchar(80) not null,
salt varchar(10) not null,
created_at datetime,
updated_at datetime null
);
```